import React from 'react'
import {
  NavbarContainer,
  HamburguerImage,
  HeaderLogo,
  Spacer,
  IconContainer,
  Icon,
  IconLabel,
  Bar,
  Notification
} from '../styles/NavbarStyles'

import Hamburguer from '../svgs/icon_hamburguer.svg'
import ContactIcon from '../svgs/paper-plane.svg'
import SearchIcon from '../svgs/search-solid.svg'
import ShoppingIcon from '../svgs/shopping-bag-solid.svg'
import N1Logo from '../img/logo_header.png'

export default function Navbar() {
  return (
    <NavbarContainer>
      <HamburguerImage src={Hamburguer} />
      <HeaderLogo src={N1Logo} />
      <Spacer />
      <IconContainer>
        <Icon src={ContactIcon} />
        <IconLabel>Contato</IconLabel>
      </IconContainer>
      <Bar />
      <IconContainer>
        <Icon src={SearchIcon} search/>
        <IconLabel search>Procurar</IconLabel>
      </IconContainer>
      <Bar />
      <IconContainer shopping>
        <Icon src={ShoppingIcon}/>
        <Notification>6</Notification>
      </IconContainer>
    </NavbarContainer>
  )
}
