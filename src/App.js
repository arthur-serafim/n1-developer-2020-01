import React from 'react';
import Navbar from './components/Navbar'
import { Container, Header } from './styles/AppStyles'

export default function App() {
  return (
    <Container>
      <Header>
        <Navbar />
      </Header>
    </Container>
  );
}
