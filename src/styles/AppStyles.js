import styled from "styled-components";
import PrincipalBanner from '../img/principal_banner_desktop.jpg'

export const Container = styled.div`
  height: 100%;
  width: 100%;
  min-height: 100vh;
`;

export const Header = styled.div`
  z-index: -1;
  padding: 20px 360px;
  box-sizing: border-box;
  margin-top: -5px;
  height: 850px;
  width: 100%;
  background-image: url(${PrincipalBanner});
  background-position: center;
  background-repeat: no-repeat;
  object-fit: cover;
`
