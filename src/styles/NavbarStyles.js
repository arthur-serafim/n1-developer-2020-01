import styled from "styled-components";

export const NavbarContainer = styled.nav`
  width: 100%;
  height: 80px;
  display: flex;
  align-items: center;
  justify-content: space-between;
`

export const HamburguerImage = styled.img`
  height: 25px;
  object-fit: cover;
  cursor: pointer;
  z-index: 10;
  margin-right: 45px;

  &:hover {
    opacity: 0.75;
  }
`

export const HeaderLogo = styled.img`
  height: 40px;
  object-fit: cover;
`

export const Spacer = styled.div`
  width: 100%;
`

export const Bar = styled.div`
  width: 2px;
  margin: 5px 20px 0 20px;
  height: 35px;
  background-color: white;
`

export const IconContainer = styled.div`
  display: flex;
  align-items: ${props => props.shopping ? 'center': 'flex-end'};
  cursor: pointer;

  &:hover {
    opacity: 0.75
  }
`

export const Icon = styled.img`
  height: ${props => props.search ? '20px' : '25px'};
  margin-top: ${props => props.search && '5px'};
  object-fit: cover;
  margin-right: 2px;
`

export const IconLabel = styled.span`
  font-size: 16px;
  text-transform: uppercase;
  color: white;
  margin-left: ${props => props.search && '5px'};
`

export const Notification = styled.div`
  height: 19px;
  width: 19px;
  font-size: 13px;
  border-radius: 50%;
  background-color: #3ec6e0;
  color: white;
  display: flex;
  align-items: center;
  justify-content: center;
  margin-left: 5px;
`
